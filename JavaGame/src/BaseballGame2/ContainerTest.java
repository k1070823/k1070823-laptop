package BaseballGame2;
import java.awt.*;
import java.applet.*;

public class ContainerTest extends Applet{
	Button button1, button2;
	Label label;
	Panel p1,p2;
	
	public void init(){
		p1 = new Panel();
		p1.setBackground(Color.cyan);
		button1 = new Button("버튼 #1");
		p1.add(button1);
		button2 = new Button("버튼 #2");
		p1.add(button2);
		add(p1);
		
		p2 = new Panel();
		p2.setBackground(Color.red);
		label = new Label("레이블",Label.CENTER);
		label.setBackground(Color.yellow);
		p2.add(label);
		add(p2);
		
	}
}
