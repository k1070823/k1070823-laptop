package BaseballGame2;
import java.awt.*;
import java.applet.*;

public class NullLayoutTest extends Applet {
	
	Label label;
	Button button;
	TextField textfield;
	Checkbox checkbox;
	
	public void init(){
		setLayout(null);
		
		button = new Button("내 버튼");
		button.setBounds(150,20,100,20);
		add(button);
		
		label = new Label("내 레이블", Label.CENTER);
		label.setBackground(Color.yellow);
		label.setBounds(150,50,100,20);
		add(label);
		
		textfield = new TextField("내 텍스트 필드");
		textfield.setBounds(150,80,100,20);
		add(textfield);
		
		checkbox = new Checkbox("내 체크박스",true);
		checkbox.setBounds(150,110,100,20);
		add(checkbox);
		
	}
}
