package BaseballGame2;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class ScrollbarTest extends Applet implements AdjustmentListener {

	Label label;
	Scrollbar scrollbar;
	
	public void init(){
		setLayout(new BorderLayout());
		
		scrollbar= new Scrollbar(Scrollbar.HORIZONTAL, 50 , 0, 1, 100);
		scrollbar.addAdjustmentListener(this);
		add("NORTH", scrollbar);
		
		label=new Label();
		label.setText("스크롤바를 조정하세요");
		label.setAlignment(Label.CENTER);
		label.setBackground(Color.yellow);
		add("Center",label);
	}
	@Override
	public void adjustmentValueChanged(AdjustmentEvent e) {
		if(e.getSource()==scrollbar){
			label.setText("위치:"+scrollbar.getValue());
		}
		
	}

}
