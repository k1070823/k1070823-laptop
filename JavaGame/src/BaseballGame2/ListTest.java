package BaseballGame2;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class ListTest extends Applet implements ActionListener, ItemListener {
	
	Label label;
	List  list;
	
	public void init(){
		list= new List(3,false);
		list.add("C언어");
		list.add("C++ 언어");
		list.add("자바");
		list.add("루비");
		list.addActionListener(this);
		list.addItemListener(this);
		add(list);
		
		label = new Label();
		label.setText("리스트 항목을 선택해 주세요");
		label.setAlignment(Label.CENTER);
		label.setBackground(Color.yellow);
		add(label);
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==list){
			label.setText("더블클릭:"+list.getSelectedItem());
		}
	}
	
	public void ItemStatedChanged(ItemEvent e){
		if(e.getSource()==list){
			label.setText("클릭:"+list.getSelectedItem());
		}
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
