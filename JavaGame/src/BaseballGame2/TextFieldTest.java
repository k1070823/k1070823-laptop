package BaseballGame2;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class TextFieldTest extends Applet implements ActionListener, TextListener {
	
	Label label;
	TextField textfield;
	
	public void init(){
		textfield = new TextField(20);
		textfield.addActionListener(this);
		textfield.addTextListener(this);
		add(textfield);
		
		label = new Label();
		label.setText("글을 입력해 주세요	");
		label.setAlignment(Label.CENTER);
		label.setBackground(Color.yellow);
		add(label);
		}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==textfield){
			label.setText("입력 완료!");
		}
		
		}@Override
	public void textValueChanged(TextEvent e) {
			if(e.getSource()==textfield){
				label.setText("입력"+textfield.getText());
			}
	

	
	}
	
}
