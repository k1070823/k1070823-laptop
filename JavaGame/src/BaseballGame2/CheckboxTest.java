package BaseballGame2;
import java.awt.*;
import java.applet.*;
import java.awt.event.*;
public class CheckboxTest extends Applet implements ItemListener {

	Label label;
	Checkbox checkbox1, checkbox2, checkbox3;
	
	public void init(){
		checkbox1 = new Checkbox();
		checkbox1.setLabel("C 언어");
		checkbox1.addItemListener(this);
		add(checkbox1);
		
		checkbox2 = new Checkbox("C++언어");
		checkbox2.addItemListener(this);
		add(checkbox2);
		
		checkbox3 = new Checkbox("자바", true);
		checkbox3.addItemListener(this);
		add(checkbox3);
		
		label = new Label();
		label.setText("체크박스를 선택 하세요");
		label.setAlignment(label.CENTER);
		label.setBackground(Color.yellow);
		add(label);
		}
	
	public void ItemStateChanged(ItemEvent e){
		if(e.getSource() == checkbox1){
			label.setText("선택: C 언어");
		}else if(e.getSource()== checkbox2){
			label.setText("선택 : C++ 언어");
		}else if(e.getSource()==checkbox3){
			label.setText("선택: 자바");
		}
		
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		
	}
}
