package BaseballGame2;
import java.awt.*;
import java.applet.*;

public class BorderLayoutTest extends Applet {

	String [] area	= {"East","West","South","North"};
	
	public void init(){
		setLayout(new BorderLayout(0,3));
		
		for(int i=0; i<5; i++){
			add(area[i], new Button(area[i]));
		}
	}
}
