package BaseballGame2;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
public class TextAreaTest extends Applet implements TextListener {

	Label label;
	TextArea textarea;
	
	public void init(){
		setLayout(new BorderLayout());
		textarea = new TextArea();
		textarea.addTextListener(this);
		add("CENTER",textarea);
		
		label = new Label();
		label.setText("글을 입력해 주세요!");
		label.setAlignment(Label.CENTER);
		label.setBackground(Color.yellow);
		add("South",label);
	}
	
	
	
	@Override
	public void textValueChanged(TextEvent e) {
		if(e.getSource()==textarea){
			label.setText("입력:"+textarea.getText());
		}
	}

}
