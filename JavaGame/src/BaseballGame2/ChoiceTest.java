package BaseballGame2;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class ChoiceTest	extends Applet implements ItemListener {
	
	Label label;
	Choice choice;
	
	public void init(){
		choice = new Choice();
		choice.addItem("C언어");
		choice.addItem("C++언어");
		choice.addItem("자바");
		choice.addItemListener(this);
		add(choice);
		
		label = new Label();
		label.setText("초이스할 항목을 선택해 주세요");
		label.setAlignment(Label.LEFT);			//Label? label?
		label.setBackground(Color.yellow);
		add(label);
		
	}
	
	public void ItemStateChanged(ItemEvent e){
		if(e.getSource()==choice){
			label.setText("선택:"+choice.getSelectedItem());
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
