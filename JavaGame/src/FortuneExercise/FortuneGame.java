package FortuneExercise;
import java.util.Date;
import java.util.Random;
import java.text.SimpleDateFormat;

public class FortuneGame {
	public static void main(String[] args) {
		Date today = new Date(); //Date의 클래스의 객체 생성 인스턴스화 시켜서 인스턴스를 만듬
		SimpleDateFormat dateForm = new SimpleDateFormat("yyyy년 mm월 dd일의"); // 오늘 날짜를 어떻게 출력할 것인지에 대한 양식
		System.out.print(dateForm.format(today)); // print()는 문자열을 출력하고 줄을 바꾸지 않는 명령
		
		Random R = new Random();
		int randomNum= Math.abs(R.nextInt()%100);
		System.out.println("금전운(100):"+ randomNum + "%" );
	}
}
