package RockSiccersPapersExercise;
import java.io.*;

public class KeyboardText {
	public static void main(String[] args) throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in); //InputStream reader 객체에 리더를 연결, 문자열 저수지에 스트림(파이프 연결) - 그 다음 물탱크인 Buffered에 저장
		BufferedReader in = new BufferedReader(isr); // 리더에 다시 버퍼를 연결
		
		while(true){
		String str;
		System.out.println("글씨를 입력하면 따라합니다.");
		str = in.readLine();
		System.out.println(str);
		}
	}

}
/*개념을 잘 알아두자 문자열 저수지(예를 들면 문자,문자열,키보드 입력 값,모니터 출력 값)에 스트림(파이프 Stream InputStream(System.in)을 연결해서
  streamReader가 그걸 정화 (예를 들어 다른언어를 우리 언어로) 한 다음 물탱크인 BufferedReader에 저장한다.
   개념숙지 요망.
*/