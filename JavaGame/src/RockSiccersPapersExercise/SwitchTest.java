package RockSiccersPapersExercise;

public class SwitchTest {
	public static void main(String[] args) {
		int num =2 ;
		
		
		switch(num){
			case 1: 
				System.out.println("결과는 1 입니다.");
				break;
			case 2:
				//break; 가 없어서 다음까지 연이어 실행.
				
			case 3:
				System.out.println("결과는 2또는 3 입니다.");
				break;
			default:
				System.out.println();
				break;
		}
	}

}
