package RockSiccersPapersExercise;
import java.util.*;
import java.io.*;

public class GameJava2 {
	public static void main(String[] args) throws IOException {
		
		
		//0~2  사이의 랜덤한 수를 구한다
		Random random = new Random();
		int compute = Math.abs(random.nextInt()%3); // 3으로 나눈 나머지 값
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		String user;
		System.out.println("가위(a) 바위(b) 보(c) 중 하나를 선택 하세요");
		user= in.readLine(); //user로 부터 키보드 값을 받는다.
		
		
		if(user.equals("a")){					//a는 가위 b는 바위 C는 보
			if(compute==0){						//가위
				System.out.println("무승부 입니다.");	
			}else if(compute==1){
				System.out.println("승리 입니다.");
			}else if(compute==2){
				System.out.println("패배 입니다.");
			}
		}else if(user.equals("b")){
			if(compute==0){
				System.out.println("승리 입니다.");
			}else if(compute==1){
				System.out.println("무승부 입니다.");
			}else if(compute==2){
				System.out.println("패배 입니다.");
			}
		}else if(user.equals("c")){
			if(compute==0){
				System.out.println("승리 입니다.");
			}else if(compute==1){
				System.out.println("패비 입니다.");
			}else if(compute==2){
				System.out.println("무승부 입니다.");
			}
		}
	}
}
