package LifeTest;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class LineTest extends Applet implements ActionListener {
	
	Label label1, label2 ,label3, label4;
	Button button;
	TextField textfield1, textfield2, textfield3, textfield4;
	
	public void inint(){
		label1 = new Label("X1:");
		add(label1);
		textfield1 = new TextField("0",3);
		add(textfield1);
		
		label2 = new Label("Y1:");
		add(label2);
		textfield2 = new TextField("0",3);
		
		label3 = new Label("X2:");
		add(label3);
		textfield3 = new TextField("0",3);
		
		label4 =new Label("Y2:");
		add(label4);
		textfield4 = new TextField("0",3);
		
		button = new Button("�׸���");
		button.addActionListener(this);
		add(button);
	}
	
	public void paint(Graphics g){
		int x1 = Integer.valueOf(textfield1.getText()).intValue();
		int y1 = Integer.valueOf(textfield2.getText()).intValue();
		int x2 = Integer.valueOf(textfield3.getText()).intValue();
		int y2 = Integer.valueOf(textfield4.getText()).intValue();

		g.drawLine(x1,y1,x2,y2);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==button){
			repaint();
		}
	}

}
