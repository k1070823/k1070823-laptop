package LifeTest;
import java.awt.*;
import java.applet.*;

public class FontTest extends Applet {

	Font font1, font2, font3;
	
	public void init(){													//Applet�� �����ҋ� �ʱ�ȭ ���ִ�  init() �޼ҵ�
		font1 = new Font("serif", Font.BOLD + Font.ITALIC, 12);
		font2 = new Font("SansSerif", Font.ITALIC, 16);
		font3 = new Font("Monospaced", Font.BOLD, 20);
		}
	
	public void paint(Graphics g){
		
		g.setColor(Color.red);
		g.setFont(font1);
		g.drawString("Serif, Bold and Italic, 12 points", 10 ,20);
		
		g.setColor(Color.blue);
		g.setFont(font2);
		g.drawString("SansSerif, Italic, 16 points", 10,50);
		
		g.setColor(Color.black);
		g.setFont(font3);
		g.drawString("Monospaced, bold ,20 points",10 , 80);
	}
}
