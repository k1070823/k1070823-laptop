package Arithmetic;
import java.util.*;
import java.io.*;

public class ArithmeticGame {
	public static void main(String[] args) throws IOException	 {
		int x,y;
		Random R = new Random();
		
		if(args.length==1){	//인수가 있으면 인수로 준 값으로 문제 출제
			x = Integer.valueOf(args[0]).intValue(); //인수는 String형이므로 랩퍼 클래스를 사용하여 int형으로 형 변환
		}else{
			x = Math.abs(R.nextInt()%9)+1;
		}
			y=Math.abs(R.nextInt()%9)+1;
			
			int num = x * y;
			
			System.out.println();
			System.out.println("*구구단"+x+"단에 대한 문제 입니다.");
			System.out.println();
			
			System.out.println(x + "*" + y + "=");
			
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String user;
			user = in.readLine(); //키보드로 부터 입력
			
			int inputNum = new Integer(user).intValue(); //키보드에서 입력 받은 값은 String형이므로 랩퍼 클래스를 사용하여 Int형으로 변환
			
			if(num==inputNum){
				System.out.println("정답 입니다.");
				}else{
					System.out.println("틀렸습니다. 정답은 " + num + "입니다.");
				}
			
	}

}
