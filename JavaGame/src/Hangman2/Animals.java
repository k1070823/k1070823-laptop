package Hangman2;

public class Animals {
	String name;
	int age;
	
	public Animals(){
		this("동물", 0);
	}
	
	public Animals(String name,int age){
		this.name= name;
		this.age = 	age;
	}
	
	public void print(){
		System.out.println("이름:"+name);
		System.out.println("나이:"+age + "세");
	}
	
	
}
