package Hangman2;

public class ConstructorTest {
	public static void main(String[] args) {
		Animals animal = new Animals();
		animal.print();
		
		Human human = new Human();
		human.print();
		
		Boy boy = new Boy();
		boy.print();
		
		System.out.println();
		
		Animals tiger = new Animals("ȣ����",8);
		tiger.print();
		
		Human mary = new Human("������",20);
		mary.print();
		
		Boy chulgu = new Boy("ö��",28);
		chulgu.print();
		
	}
}
