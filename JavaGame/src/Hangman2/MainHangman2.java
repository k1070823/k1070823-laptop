package Hangman2;

import java.io.IOException;

public class MainHangman2 {
	public static void main(String[] args) throws IOException {
		Hangman2 hangman = new Hangman2();		//Hangman2 객체 생성
		
		int result = hangman.playGame();       //게임을 실행
		
		System.out.println();
		if(result<=2){
			System.out.println("개고수");
		}else if(result<=3){
			System.out.println("고수");
		}else if(result<=4){
			System.out.println("낫베드");
		}else{
			System.out.println("ㄱㅎㅅ 인 듯?");
		}
		
	}

}
