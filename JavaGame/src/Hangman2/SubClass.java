package Hangman2;

public class SubClass extends SuperClass {				//superclass 상속
	public void print(){
		System.out.println("서브 클래스");
	}
	
	public void callSuperThis(){
		super.print();									//Super 클래스 메소드
		this.print();									//Sub   클래스 메소드
	}
}
