package Hangman2;

public class SuperThisTest {
	public static void main(String[] args) {
		SuperClass superclass = new SuperClass();		//SuperClass형 객체 생성
		
		System.out.println("SuperClass의 print()메소드");
		superclass.print();
		
		SubClass subclass = new SubClass();
		subclass.print();
		
		System.out.println("SubClass에서 super,this 호출");
		subclass.callSuperThis();
		
	}
}
