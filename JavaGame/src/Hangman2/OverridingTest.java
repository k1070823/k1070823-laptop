package Hangman2;

public class OverridingTest {
	public static void main(String[] args) {
		Dog yourDog = new Dog("벤지",4);					//Dog 형 객체 생성
			yourDog.bark();
			yourDog.bite("우체부");
			
			
		Snoopy myDog = new Snoopy();					//Snoopy 형 객체 생성
		myDog.bark(); 									//메소드 오버라이딩
		myDog.bite("낸시", 9);							//메소드 오버로딩
	}
}
