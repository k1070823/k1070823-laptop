package Hangman2;

public class Manager extends Employee {
	String department;
	
	public Manager(String n, int s, String d){			//생성자
		super(n,s);										//Employee 클래스의 생성자에서 이름과 월급을 저장
		department=d;									//부서 저장
	}
	
	public void getInformation(){
		System.out.println("이름:"+ name + "연봉:"+salary+ "부서:"+department);
	}													//Employee 클래스에 있는 name과 salary 변수를 사용
	
}
