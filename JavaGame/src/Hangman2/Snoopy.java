package Hangman2;

public class Snoopy extends Dog {					//Dog클래스를 상속

	public Snoopy(){								//매개 변수가 1인 bite()메소드
		super ("스누피",3);
	}
	
	public void bite(String name, int age){
		System.out.println(dog_name+ "가 문"+name+"sms "+ age+ "살 입니다.(메소드 오버로딩)");
	}
	
	public void bark(){								//Dog 클래스의 bark() 메소드와 오버라이딩 되는 매개변수가 없는 bark()메소드
		System.out.println("안녕하세요,"+dog_name+"입니다."+"매소드 오버라이딩");
	}
	
	
}
