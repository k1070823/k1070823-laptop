  package Hangman2;
import java.util.*;
import java.io.*;

public class Hangman2 extends Hangman {						//Hangman 클래스 상속
	String [] words = {"influenza",							//문제의 단어가 저장 된 배열
					   "fever",
					   "cancer",
					   "poison",
					   "victim",
					   "physician",
					   "clinic",
					   "symptom",
					   "medicine",
					   "drug",
					   "hygiene",
					   };
	
	String [] meaning = {
					  	"독감",							//문제 단어가 뜻이 저장 된 배열
						"열",
						"암",
						"독",
						"환자",
						"내과의사",
						"진료소",
						"징후",
						"의학",
						"약",
						"위생학",
					  };
	
	public Hangman2() throws IOException{
		Random r = new Random();						
		int randomNum = Math.abs(r.nextInt()%words.length); //임의의 난수 발생
		
		hiddenString = words[randomNum];					//문제가 저장 된 배열 중 하나 선택
		
		System.out.println("\n 의미:"+ meaning[randomNum]);
	}
		
	
	public char readChar() throws IOException{
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String user;
			
			do{
				System.out.println("1 문자를 입력하세요(힌트를 원하면 ? 입력)");
				user=in.readLine();							//키보드로 부터 입력
				
				if(user.charAt(0)=='?'){					//입력 문자 ?이면
					boolean giveHint = false;
					int i=0;
					while(!giveHint){
						if(outputString.charAt(i)=='_'){	//못만친 문자 힌트 출력
							System.out.println();
							System.out.println("힌트:"+hiddenString.charAt(i));
							System.out.println();
							failed++;
							giveHint = true;}
						}
						i++;
					}
				}while(user.charAt(0)=='?');
				
				return user.charAt(0);
			}
		}
			
