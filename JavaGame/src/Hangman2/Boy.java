package Hangman2;

public class Boy extends Human {	//Human 클래스 상속
	public Boy(){
		this("소년",0);				//8행의 Human 클래스의 생성자에게 이름과 나이를 전달
	}
	
	public Boy(String name,int age){
		super(name,age);
	}
}
