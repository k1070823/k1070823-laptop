package Hangman2;
/*오버로딩(Overloading) : 같은 이름의 메소드를 여러 개 가지면서 매개변수의 유형과 개수가 다르도록 하는 기술
오버라이딩(Overriding) : 상위 클래스가 가지고 있는 메소드를 하위 클래스가 재정의 해서 상요한다.*/
public class Dog {
	String dog_name; 							//강아지 이름
	int dog_age;								//나이
	
	public Dog(String name, int age){			//생성자
		dog_name = name;
		dog_age  =  age;    
	}
	
	public void bite(String name){				//매개 변수가 1인 메소드
		System.out.println(dog_name+"가" + name +"을 물었습니다");
	}
	
	public void bark(){							//매개 변수가 없는 메소드
		System.out.println("멍멍");
	}
}
