package Hangman;

public class PointTest {
	public static void main(String[] args) {
		Point p = new Point(); //point 클래스의 객체 생성
		p.x=10;				   //point 클래스 멤버 변수 x에 저장
		p.y=20;				   //point 클래스 멤버 변수 y에 저장
		p.printXY();			//point 클래스의 PointXy() 메소드 실행
		
	}

}
