package Hangman;

public class Clock {
	
	int hour;
	int min;
	int sec;
	
	public Clock(int h,int m,int s){  //생성될때 h,m,s를 인수로 받음
		hour=h;
		min =m;
		sec= s;
	}
	
	public void printTime(){
		System.out.println(hour + ":" + min + ":" + sec);
	}
}
