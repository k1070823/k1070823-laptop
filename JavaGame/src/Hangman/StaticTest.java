package Hangman;

public class StaticTest {
	public static void main(String[] args) {
		MyClass[] mc = new MyClass[10];			//10개의 MyClass 타입을 갖는 배열 생성
		
		for(int i=0; i<10; i++){
			mc[i] = new MyClass();				//MyClass클래스의 객체를 생성
			System.out.println("MyClass Instace의 수 :" + mc[0].getObjectNum()+"개");
			//MyClass 클래스의 getObjectNum() 메소드를 호출하여 object_num 변수의 값을 출력
		}
		
	}
}
