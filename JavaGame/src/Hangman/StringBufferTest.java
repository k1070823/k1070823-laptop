package Hangman;

public class StringBufferTest {
	public static void main(String[] args) {
		StringBuffer sb= new StringBuffer();		//StringBuffer형 변수 생성
		sb.append('R'); 							//char 형
		sb.append("ealty");							//String 형
		sb.append(2007);							//int 형
		sb.append(' ');								
		sb.append(7.5);								//double 형
		System.out.println(sb);
		sb.insert(6, ", B");						//String 형
		sb.insert(9, 'c');							//char 형
		System.out.println(sb);
	}
}
