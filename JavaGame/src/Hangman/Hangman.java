package Hangman;
import java.io.*;
public class Hangman {
	String hiddenString;					//숨긴 문자열  문제 답
	StringBuffer outputString;				//플레이어가 입력에 따른 결가로 보여줄 문자열
	StringBuffer inputString;				//플레이어가 입력한 문자들의 모임
	int remainder;							//맞출 문자열(남아있는 문자열)
	int failed;								//실패 횟 수
	
	
	public Hangman() throws IOException{
		hiddenString="hello";							//문제가 Hello
	}
	
	public int playGame() throws IOException{
		outputString = new StringBuffer();
		
		for(int i=0; i<hiddenString.length(); i++){		//hiddenString 문자 수만큼 '_'	출력
			outputString.append('-');
		}
		
		inputString = new StringBuffer();
		
		remainder = hiddenString.length();				//hiddenString의 문자 수가 맞출 문제의 문자 수
		failed = 0;
		System.out.println("\n단어(" + hiddenString.length()+ "글자" +"): "+outputString);
		drawMan();										//교수대 그리기
		
		do{
			checkChar(readChar());						//한 문자를 입력 받아서 정답인지 확인
			System.out.println("\n단어(" + hiddenString.length()+ "글자" +"): "+outputString);
			drawMan();
			}while((remainder>0)&&(failed<6));
		
		return failed;
	}
	
	public void checkChar(char guess)
	{
		boolean already = false;
		for(int i=0; i<inputString.length(); i++){
			if(inputString.charAt(i)==guess){			//이미 입력한 것인지 확인
				System.out.println("\n 이미 입력한 문자 입니다! 다시 입력해 주세요");
				already = true;
			}
		}
		
		if(!already){	
			
			inputString.append(guess);			//입력한 문자들의 모임에 추가
			
			boolean success =false;
			for(int i=0; i<hiddenString.length(); i++){
				if(hiddenString.charAt(i)==guess){		//문제에 해당 문자가 있는지 조사
					outputString.setCharAt(i, guess);	//문제에 문자가 있으면 -를 문자로 변경
					remainder--;
					success = true;
				}
			}
			if(!success) failed++;						//입력한 문자가 문제에 없으면 실패 횟수 1 증가
		}
	}
	
	public void drawMan(){
			System.out.println("____________");
			System.out.println("|          |");
		switch(failed){									//실패횟수에 따라 교수대에 사람 그리기
		case 0 :
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			
			
		case 1:
			System.out.println("     O     |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			break;
		case 2:
			System.out.println("     O     |");
			System.out.println("  ___|     |");
			System.out.println("     |     |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			break;
		case 3:
			System.out.println("     O     |");
			System.out.println("  ___|___  |");
			System.out.println("     |     |");
			System.out.println("           |");
			System.out.println("           |");
			System.out.println("           |");
			break;
		case 4:
			System.out.println("     O     |");
			System.out.println("  ___|___  |");
			System.out.println("     |     |");
			System.out.println("    |      |");
			System.out.println("    |      |");
			System.out.println("           |");
			break;
			
		case 5:
			System.out.println("     O     |");
			System.out.println("  ___|___  |");
			System.out.println("     |     |");
			System.out.println("    | |    |");
			System.out.println("    | |    |");
			System.out.println("           |");
			break;
			
		case 6:
			System.out.println("     O     |");
			System.out.println("  ___|___  |");
			System.out.println("     |     |");
			System.out.println("    | |    |");
			System.out.println("    | |    |");
			System.out.println("  죽음  ㅂㅂ    |");
			break;
		}
	}
	
	public char readChar() throws IOException{
		BufferedReader in = new BufferedReader (new InputStreamReader(System.in));
		String user;
		
		System.out.println("1 문자를 입력 하세요");
		user = in.readLine();	//키보드로 부터 한 줄을 입력
		return user.charAt(0);	//입력받은 문자열 중 첫번쩨 값 반환
		
	}
	
}
