package Hangman;
import java.io.*;
public class ReverseString {
	public static void main(String[] args) throws IOException {
		System.out.println("거꾸로 뒤집을 문자를 입력해 주세요.");
		
		BufferedReader in =new BufferedReader(new InputStreamReader(System.in));
		String str  = in.readLine(); //키보드로 부터 입출력
		
		StringBuffer sb= new StringBuffer(str);
		System.out.println(sb.reverse());
	}


}
